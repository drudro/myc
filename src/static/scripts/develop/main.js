$(window).resize(function () {
	stickFooter();
	stickHeader();
	$('.slider').each(function (i, item) {
		if ($(item).hasClass('slick-initialized')) {
			var curSlide = $(item).slick('slickCurrentSlide');
			$(item).slick('slickGoTo', curSlide);
		}
	});
});
$(window).scroll(function () {
	stickHeader();
});
$(window).on("load", function () {
	stickFooter();
	stickHeader();
	$('select').each(function () {
		$(this).select2({
			minimumResultsForSearch: Infinity,
			placeholder: function () {
				$(this).data('placeholder');
			}
		});
		$(this).next().addClass($(this).data("class"));
	});
	
	$('.datepicker').datepicker();
});