$(function() {
    var $accordionLinks = $('.js-accordion'),
        $accordionConts = $('.js-accordion-cont');
    $accordionConts.hide();
    $('.js-accordion').click(function(e) {
        e.preventDefault();
        
        var $accordionLink = $(this),
            $accordionCont = this.hash? $(this.hash) : $(this).next('.js-accordion-cont');

        if(!$accordionCont.hasClass('dropped')){            
            $accordionCont.slideDown(300).addClass('dropped');
            $accordionLink.addClass('dropped');            
        } else {
            $accordionCont.slideUp(300).removeClass('dropped');
            $accordionLink.removeClass('dropped');              
        }
     
        $accordionConts.not($accordionCont).slideUp(300).removeClass('dropped');
        $accordionLinks.not($accordionLink).removeClass('dropped');     
        
        $(window).resize();

        return false;
    });



    // $('select').select2();

});