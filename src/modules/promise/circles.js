let circles = $('.circle.promise');
circles.each((i,circ)=>{
	$(circ).circleProgress({
		value: 0,
		size: $(circ).data("size") || $(circ).width(),
		lineCap:'square',
		startAngle:Math.PI*1.5,
		thickness:5,
		emptyFill: $(circ).data("empty-fill") || "#d6d7d6"
	  }).on('circle-animation-progress', function(event, progress, stepValue) {
		$(this).find('strong').html(`${Math.round(100 * stepValue)}${$(circ).data("suffix") || "%"}`).css("color",$(circ).data("label-color"));
	  });
})

$(window).scroll(()=>{
	circles.each((i,circ)=>{
		if($(circ).visible() && !$(circ).hasClass('fixValue')) $(circ).circleProgress({value:$(circ).data("value")}).addClass('fixValue');
	})
})