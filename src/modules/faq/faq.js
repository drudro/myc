$(function () {
	function FullTextContains(innerText, searchTerm) {
		for (var x = 0; x < searchTerm.length; x++) {
			if (innerText.toLowerCase().indexOf(searchTerm[x].toLowerCase()) >= 0) {
				return true;
			}
		}
		return false;
	}


	$('.faq').on('keyup input', function (e) {
		var text = $.trim($(this).val());
		if (e.keyCode == 13) {
			SearchFAQ(text);
		} else if (text == '') {
			SearchFAQ('');
		}
	});

	$('#btnFaqSearch').on('click', function (e) {
		var text = $.trim($('.faq').val());
		SearchFAQ(text);
	});

	function SearchFAQ(searchTermText) {
		var searchTerm = searchTermText.split(' ');
		if (searchTermText != '') {
			$(".question").filter(function (index) {
				var panelText = $.trim($(this).text());
				if (FullTextContains(panelText, searchTerm) == true) {
					//console.log("found it");
					return true;
				} else {
					//console.log("not found");
					$(this).slideUp();
					return false;
				}
			}).slideDown();
		} else {
			$(".question").slideDown();
		}
	}
});